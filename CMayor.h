#ifndef CMAYOR_H
#define CMAYOR_H


class CMayor
{
    public:
        float m_rMa;
        float m_aMa;

    public:

        CMayor();

        CMayor( float valor );
        CMayor(float rMa, float aMa);

        virtual ~CMayor();

        int GetrMa() { return m_rMa; }
        void SetrMa(float val) { m_rMa = val; }

        int GetaMa() { return m_aMa; }
        void SetaMa(float val) { m_aMa = val; }

        double CalCMa();
};

#endif // CMAYOR_H
