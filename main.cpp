#include <iostream>
#include "CMayor.h"
#include "CMenor.h"
#include "AMayor.h"
#include "AMenor.h"
#include "AlcoholVol.h"

using namespace std;


    // GALINDO MENDOZA GEOVANNI ISRAEL
    // 2EV10
    // PROGRAMA PARA CALCULAR LAS DIMENSIONES DE UNA BOTELLA
    // PRACTICA 1


int main()
{
    CMayor CMayor(4, 21);
    CMenor CMenor(1.3, 7);
    AMayor AMayor(4, 1);
    AMenor AMenor(1.3, 1);
    AlcoholVol AlcoholVol(0.10, 1);


    cout << "Las dimensiones de la figura son : " << endl;
    cout << "Radio Mayor = " << CMayor.m_rMa << endl;
    cout << "Radio Menor = " << CMenor.m_rMe << endl;
    cout << "Altura Mayor = " << CMayor.m_aMa << endl;
    cout << "Altura Menor = " << CMenor.m_aMe << endl << endl << endl;

    cout << "El Volumen del Cilindro Mayor es : " << CMayor.CalCMa() << endl;
    cout << "El Volumen del Cilindro Menor es : " << CMenor.CalCMe() << endl;
    cout << "El Volumen Total de la Botella es : " << CMayor.CalCMa() + CMenor.CalCMe() << endl << endl;

    cout << "El Area del Circulo Mayor es: " << AMayor.CalAMa() << endl;
    cout << "El Area del Circulo Menor o Tapa es: " << AMenor.CalAMe() << endl << endl;

    cout << "La cantidad de alcohol en la Botella es : " << AlcoholVol.CalAlcVol() * (CMayor.CalCMa() + CMenor.CalCMe()) << endl << endl;

    return 0;
}

