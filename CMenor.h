#ifndef CMENOR_H
#define CMENOR_H


class CMenor
{
    public:
        float m_rMe;
        float m_aMe;

    public:

        CMenor();

        CMenor( float valor );
        CMenor( float rMe, float aMe );

        virtual ~CMenor();

        int GetrMe() { return m_rMe; }
        void SetrMe(float val) { m_rMe = val; }

        int GetaMe() { return m_aMe; }
        void SetaMe(float val) { m_aMe = val; }


        double CalCMe();
};

#endif // CMENOR_H
