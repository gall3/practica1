#ifndef AMENOR_H
#define AMENOR_H


class AMenor
{
    public:
        float m_raMe;
        float m_alMe;

    public:

        AMenor();

        AMenor( float valor );
        AMenor( float raMe, float alMe );

        virtual ~AMenor();

        int GetraMe() { return m_raMe; }
        void SetraMe(float val) { m_raMe = val; }

        int GetalMe() { return m_alMe; }
        void SetalMe(float val) { m_alMe = val; }


        double CalAMe();
};

#endif // AMENOR_H
