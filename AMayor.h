#ifndef AMAYOR_H
#define AMAYOR_H


class AMayor
{
    public:
        float m_raMa;
        float m_alMa;

    public:

        AMayor();

        AMayor( float valor );
        AMayor( float raMa, float alMa);

        virtual ~AMayor();

        int GetraMa() { return m_raMa; }
        void SetraMa(float val) { m_raMa = val; }

        int GetalMa() { return m_alMa; }
        void SetalMa(float val) { m_alMa = val; }


        double CalAMa();
};

#endif // AMAYOR_H
