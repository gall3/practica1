#ifndef ALCOHOLVOL_H
#define ALCOHOLVOL_H


class AlcoholVol
{
    public:
        float m_AlcVol;
        float m_x;

    public:

        AlcoholVol();

        AlcoholVol( float valor );
        AlcoholVol( float AlcVol, float x);
        virtual ~AlcoholVol();

        int GetAlcVol() { return m_AlcVol; }
        void SetAlcVol(float val) { m_AlcVol = val; }

        int Getx() { return m_x; }
        void Setx(float val) { m_x = val; }


        double CalAlcVol();
};

#endif // ALCOHOLVOL_H
